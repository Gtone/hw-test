#!/bin/bash

docker network create --subnet=172.100.0.0/16 herdwatch-network
docker network connect herdwatch-network backend_nginx
docker network connect herdwatch-network client_php
#echo "172.100.0.0.1 herdwatch" >> /etc/hosts
