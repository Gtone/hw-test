<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 * @ORM\Table(name="`user`")
 * @UniqueEntity("email", message="This email is already exists")
 */
class User implements \JsonSerializable
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="User name can not be blank")
     * @Assert\Length(min=2, max=254, minMessage="Name value is too short", maxMessage="Name value is too long")
     * @Assert\Type(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(message="Email can not be blank")
     * @Assert\Length(min=1, max=100)
     * @Assert\Type(type="string")
     * @Assert\Email(message="Wrong email format")
     */
    private $email;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Group", inversedBy="users")
     */
    private $groups;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $email
     * @return User
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @return Collection|null
     */
    public function getGroups(): ?Collection
    {
        return $this->groups;
    }

    /**
     * @return Collection|null
     */
    public function getMyGroups(): ?Collection
    {
        return $this->groups;
    }

    /**
     * @param Group $userGroup
     * @return User
     */
    public function addMyUserGroup(Group $userGroup): self
    {
            $this->groups[] = $userGroup;
            $userGroup->addUser($this);

        return $this;
    }

    /**
     * @param Group $userGroup
     * @return User
     */
    public function removeMyGroup(Group $userGroup): self
    {
        if ($this->groups->contains($userGroup)) {
            $this->groups->removeElement($userGroup);
        }

        return $this;
    }

    /**
     * Specify data which should be serialized to JSON
     * @link https://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     * @since 5.4.0
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'groups' => $this->groups->map(function (Group $group) {
                return $group->customJsonSerialize();
            })->toArray()
        ];
    }

    /**
     * @return array
     * custom JSON serialize for avoiding circular references
     */
    public function customJsonSerialize() {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email
        ];
    }
}
