<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 28.07.20
 * Time: 20:06
 */

namespace App\Model;
use App\Entity\Group as GroupEntity;


class Group
{
    public function createGroup(array $data) {
        $group = new GroupEntity();

        $group->setName($data['name']);

        return $group;
    }

    public function updateGroup(GroupEntity $group, $data) {
        if(array_key_exists('name', $data)) {
            if (!is_null($data['name'])) {
                $group->setName($data['name']);
            }
        }

        return $group;
    }
}