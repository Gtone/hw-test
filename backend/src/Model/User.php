<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 26.07.20
 * Time: 16:36
 */

namespace App\Model;
use App\Entity\User as UserEntity;


class User
{
    public function createUser(array $data) {
        $user = new UserEntity();

        $user->setName($data['name']);
        $user->setEmail($data['email']);

        return $user;
    }

    public function updateUser(UserEntity $user, $data) {
        if(array_key_exists('name', $data)) {
            if (!is_null($data['name'])) {
                $user->setName($data['name']);
            }
        }

        if(array_key_exists('email', $data)) {
            if (!is_null($data['email'])) {
                $user->setEmail($data['email']);
            }
        }

        return $user;
    }
}