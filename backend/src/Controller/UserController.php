<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 26.07.20
 * Time: 18:49
 */

namespace App\Controller;


use App\Model\User;
use App\Repository\UserRepository;
use App\Service\ConstraintValidator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\User as UserEntity;
use Symfony\Component\Serializer\SerializerInterface;

class UserController
{
    /**
     * @var
     */
    private $user;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ConstraintValidator
     */
    private $constraintValidator;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var SerializerInterface
     */
    private $serializer;

    public function __construct(
        User $user,
        UserRepository $userRepository,
        EntityManagerInterface $entityManager,
        ConstraintValidator $constraintValidator,
        ValidatorInterface $validator,
        SerializerInterface $serializer
    )
    {
        $this->user = $user;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->constraintValidator = $constraintValidator;
        $this->validator = $validator;
        $this->serializer = $serializer;
    }

    public function getAction(UserEntity $user) {
        $userData = $this->serializer->serialize($user->customJsonSerialize(), 'json');
        return new Response($userData, Response::HTTP_OK);
    }

    public function cgetAction() {
        $users = $this->userRepository->findAll();
        $usersData = $this->serializer->serialize($users, 'json');
        return new Response($usersData, Response::HTTP_OK);
    }

    public function postAction(Request $request)
    {
        if (empty($request->request->get('name'))) {
            return new Response('Empty name', Response::HTTP_BAD_REQUEST, array('Content-Type' => 'text/json'));
        }
        if (empty($request->request->get('email'))) {
            return new Response('Empty email', Response::HTTP_BAD_REQUEST, array('Content-Type' => 'text/json'));
        }

        $user = $this->user->createUser([
            'name' => $request->get('name'),
            'email' => $request->get('email')
        ]);

        //create validator
        $validate_results = $this->constraintValidator->getValidateResponse($this->validator->validate($user));

        if (!is_bool($validate_results)) //if returns not true - incorrect data
            return new Response(implode(",", $validate_results), 400, array('Content-Type' => 'text/json'));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return new Response('created', Response::HTTP_CREATED, array('Content-Type' => 'text/json'));
    }

    public function putAction(UserEntity $user, Request $request)
    {
        $user = $this->user->updateUser($user, [
            'name' => $request->get('name') ?? $user->getName(),
            'email' => $request->get('email') ?? $user->getEmail()
        ]);

        //create validator
        $validate_results = $this->constraintValidator->getValidateResponse($this->validator->validate($user));

        if (!is_bool($validate_results)) //if returns not true - incorrect data
            return new Response(implode(",", $validate_results), Response::HTTP_BAD_REQUEST, array('Content-Type' => 'text/json'));

        $this->entityManager->persist($user);
        $this->entityManager->flush();

        return new Response('updated', Response::HTTP_OK, array('Content-Type' => 'text/json'));
    }

    public function deleteAction(UserEntity $user) {
        $this->entityManager->remove($user);
        $this->entityManager->flush();

        return new Response('deleted', Response::HTTP_OK, array('Content-Type' => 'text/json'));
    }
}