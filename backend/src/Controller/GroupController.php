<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 28.07.20
 * Time: 20:08
 */

namespace App\Controller;

use App\Model\Group;
use App\Repository\GroupRepository;
use App\Repository\UserRepository;
use App\Service\ConstraintValidator;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use App\Entity\Group as GroupEntity;
use Symfony\Component\Serializer\SerializerInterface;
use App\Entity\User as UserEntity;


class GroupController
{
    /**
     * @var Group
     */
    private $group;
    /**
     * @var GroupRepository
     */
    /**
     * @var GroupRepository
     */
    private $groupRepository;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserRepository
     */
    private $userRepository;
    /**
     * @var ConstraintValidator
     */
    private $constraintValidator;
    /**
     * @var ValidatorInterface
     */
    private $validator;
    /**
     * @var SerializerInterface
     */
    private $serializer;


    public function __construct(
        Group $group,
        UserRepository $userRepository,
        GroupRepository $groupRepository,
        EntityManagerInterface $entityManager,
        ConstraintValidator $constraintValidator,
        ValidatorInterface $validator,
        SerializerInterface $serializer
    )
    {
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
        $this->constraintValidator = $constraintValidator;
        $this->validator = $validator;
        $this->serializer = $serializer;
        $this->group = $group;
        $this->groupRepository = $groupRepository;
    }

    public function getAction(GroupEntity $group)
    {
        $groupData = $this->serializer->serialize($group, 'json');
        return new Response($groupData, Response::HTTP_OK);
    }

    public function cgetAction()
    {
        $groups = $this->groupRepository->findAll();
        $groupsData = $this->serializer->serialize($groups, 'json');
        return new Response($groupsData, Response::HTTP_OK);
    }

    public function postAction(Request $request)
    {
        $group = $this->group->createGroup([
            'name' => $request->get('name')
        ]);

        if (empty($request->request->get('name'))) {
            return new Response('Empty name', Response::HTTP_NOT_FOUND, array('Content-Type' => 'text/json'));
        }

        //create validator
        $validate_results = $this->constraintValidator->getValidateResponse($this->validator->validate($group));

        if (!is_bool($validate_results)) //if returns not true - incorrect data
            return new Response(implode(",", $validate_results), 400, array('Content-Type' => 'text/json'));

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        return new Response('created', Response::HTTP_CREATED, array('Content-Type' => 'text/json'));
    }

    public function putAction(GroupEntity $group, Request $request)
    {
        $group = $this->group->updateGroup($group, [
            'name' => $request->get('name') ?? $group->getName()
        ]);

        //create validator
        $validate_results = $this->constraintValidator->getValidateResponse($this->validator->validate($group));

        if (!is_bool($validate_results)) //if returns not true - incorrect data
            return new Response(implode(",", $validate_results), Response::HTTP_BAD_REQUEST, array('Content-Type' => 'text/json'));

        $this->entityManager->persist($group);
        $this->entityManager->flush();

        return new Response('updated', Response::HTTP_OK, array('Content-Type' => 'text/json'));
    }

    public function deleteAction(GroupEntity $group)
    {
        $this->entityManager->remove($group);
        $this->entityManager->flush();

        return new Response('deleted', Response::HTTP_OK, array('Content-Type' => 'text/json'));
    }

    //Many to many actions

    public function addUserToGroup(GroupEntity $group, UserEntity $user) {
        $group->addUser($user);
        $this->entityManager->flush();

        return new Response('added', Response::HTTP_OK);
    }

    public function removeUserFromGroup(GroupEntity $group, UserEntity $user) {
        $user->removeMyGroup($group);

        $this->entityManager->flush();

        return new Response('removed', Response::HTTP_OK);
    }
}