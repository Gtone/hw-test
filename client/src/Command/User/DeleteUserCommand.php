<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 28.07.20
 * Time: 19:00
 */

namespace App\Command\User;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;

class DeleteUserCommand extends Command
{
    /**
     * @var string
     */
    private $siteUrl;

    public function __construct(string $siteUrl, $name = null)
    {
        parent::__construct($name);
        $this->siteUrl = $siteUrl;
    }

    protected static $defaultName = 'user:delete';

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Command for deleting existing user')
            ->setDefinition([
                new InputArgument('id', InputArgument::REQUIRED, 'The user id')
            ])
            ->setHelp(<<<'EOT'
                The <info>fos:user:update</info> command is updating a user
EOT
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = [];

        if (!$input->getArgument('id')) {
            $question = new Question('Enter the user id for profile that you want to delete:');
            $question->setValidator(function ($id) {
                if (empty($id)) {
                    throw new \Exception('User id can not be empty');
                }

                return $id;
            });
            $questions['id'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        $httpClient = HttpClient::create();
        $response = null;

        //Are you sure? There is no way to undo this action
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('<bg=yellow;options=bold>Are you sure? There is no way to undo this action!(y to proceed)</>', false);

        if (!$helper->ask($input, $output, $question)) {
            return 0;
        }
        $response = $httpClient->request('DELETE', $this->siteUrl . '/users/' . $id);

        //Check result, output success message depending on http code
        $statusCode = $response->getStatusCode();
        if ($statusCode == Response::HTTP_NOT_FOUND) {
            $output->writeln('<error>User is not found</error>');
            exit;
        }

        //return errors if we unlucky
        if ($statusCode != Response::HTTP_OK) {
            $errors = $response->getContent(false);
            $output->writeln('<error>' . $errors . '.</error>');
        } else {
            $output->writeln(sprintf('<info>User has been deleted</info>'));
        }
    }
}