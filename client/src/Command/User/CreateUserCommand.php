<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 26.07.20
 * Time: 19:37
 */

namespace App\Command\User;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class CreateUserCommand extends Command
{
    /**
     * @var string
     */
    private $siteUrl;

    public function __construct(string $siteUrl, $name = null)
    {
        parent::__construct($name);
        $this->siteUrl = $siteUrl;
    }

    protected static $defaultName = 'user:create';

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Command for creating new user')
            ->setDefinition([
                new InputArgument('username', InputArgument::REQUIRED, 'The username'),
                new InputArgument('email', InputArgument::REQUIRED, 'The email')
            ])
            ->setHelp(<<<'EOT'
                The <info>fos:user:create</info> command creates a user
EOT
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = [];

        if (!$input->getArgument('username')) {
            $question = new Question('Please choose a username:');
            $question->setValidator(function ($username) {
                if (empty($username)) {
                    throw new \Exception('Username can not be empty');
                }
                if(!is_string($username)) {
                    throw new \Exception('Wrong username format');
                }

                return $username;
            });
            $questions['username'] = $question;
        }

        if (!$input->getArgument('email')) {
            $question = new Question('Please choose an email:');
            $question->setValidator(function ($email) {
                if (empty($email)) {
                    throw new \Exception('Email can not be empty');
                }

                return $email;
            });
            $questions['email'] = $question;
        }

        foreach ($questions as $name=>$question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $email = $input->getArgument('email');
        $httpClient = HttpClient::create();
        $response = null;

        $response = $httpClient->request('POST', $this->siteUrl . '/users/create', [
            'body' => [
                'name' => $username,
                'email' => $email
            ]
        ]);

        //Check result, output success message depending on http code
        $statusCode = $response->getStatusCode();
        //return errors if we unlucky
        if ($statusCode != Response::HTTP_CREATED) {
            $errors = $response->getContent(false);
            $output->writeln('<error>' . $errors . '.</error>');
        } else {
            $output->writeln(sprintf('Created user <comment>%s</comment>', $username));
        }
    }
}