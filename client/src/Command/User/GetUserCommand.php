<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 28.07.20
 * Time: 15:19
 */

namespace App\Command\User;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;


class GetUserCommand extends Command
{
    /**
     * @var string
     */
    private $siteUrl;
    /**
     * @var null
     */
    private $name;

    public function __construct(string $siteUrl, $name = null)
    {
        parent::__construct($name);
        $this->siteUrl = $siteUrl;
        $this->name = $name;
    }

    protected static $defaultName = 'user:get';

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Command for updating existing user')
            ->setDefinition([
                new InputArgument('choice', InputArgument::REQUIRED, 'choice'),
                new InputArgument('id', InputArgument::OPTIONAL, 'The user id'),
            ])
            ->setHelp(<<<'EOT'
                The <info>fos:user:get</info> command is updating a user
EOT
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('choice')) {
            $question = new ChoiceQuestion(
                'Get one users or all?',
                ['one', 'all'],
                0
            );
            $question->setErrorMessage('Wrong selection');

            $choice = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument('choice', $choice);
            if ($choice === 'one') {
                $question = new Question('Enter id:');
                $question->setValidator(function ($id) {
                    return $id;
                });
                $answer = $this->getHelper('question')->ask($input, $output, $question);
                $input->setArgument('id', $answer);
                $input->setArgument('choice', $answer);
            }
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $httpClient = HttpClient::create();
        $response = null;
        if (!is_null($id = $input->getArgument('id'))) {
            $response = $httpClient->request('GET', $this->siteUrl . '/users/' . $id);

            $statusCode = $response->getStatusCode();
            if ($statusCode == Response::HTTP_NOT_FOUND) {
                $output->writeln('<error>User is not found</error>');
                exit;
            }

            $this->makeUsersTable($response, $output);
            exit;
        }
        $response = $httpClient->request('GET', $this->siteUrl . '/users');
        //Check result, output success message depending on http code
        $statusCode = $response->getStatusCode();
        //return errors if we unlucky
        if ($statusCode != Response::HTTP_OK) {
            $errors = $response->getContent(false);
            $output->writeln('<error>' . $errors . '.</error>');
        }
        $this->makeUsersTable($response, $output);
    }

    private function makeUsersTable($response, OutputInterface $output)
    {
        if (!is_null($response)) {
            $users = json_decode($response->getContent());
            $table = new Table($output);
            $table->setHeaders(['id', 'name', 'email']);
            if (!is_array($users)) {
                $user = json_decode($response->getContent());
                $table->setRows([[$user->id, $user->name, $user->email]]);
            } else {
                $rows = [];
                foreach ($users as $user) {
                   $rows[] = [$user->id, $user->name, $user->email];
                }
                $table->setRows($rows);
            }
            $table->render();
        }
    }
}