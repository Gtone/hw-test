<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 28.07.20
 * Time: 12:42
 */

namespace App\Command\User;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;

class UpdateUserCommand extends Command
{
    /**
     * @var string
     */
    private $siteUrl;

    public function __construct(string $siteUrl, $name = null)
    {
        parent::__construct($name);
        $this->siteUrl = $siteUrl;
    }

    protected static $defaultName = 'user:update';

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Command for updating existing user')
            ->setDefinition([
                new InputArgument('id', InputArgument::REQUIRED, 'The user id'),
                new InputArgument('username', InputArgument::OPTIONAL, 'The username'),
                new InputArgument('email', InputArgument::OPTIONAL, 'The email')
            ])
            ->setHelp(<<<'EOT'
                The <info>fos:user:update</info> command is updating a user
EOT
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = [];

        if (!$input->getArgument('id')) {
            $question = new Question('Enter the user id for profile you want to update:');
            $question->setValidator(function ($id) {
                if (empty($id)) {
                    throw new \Exception('User id can not be empty');
                }

                return $id;
            });
            $questions['id'] = $question;
        }

        if (!$input->getArgument('username')) {
            $question = new Question('Please choose a username:');
            $question->setValidator(function ($username) {
                return $username;
            });
            $questions['username'] = $question;
        }

        if (!$input->getArgument('email')) {
            $question = new Question('Please choose an email:');
            $question->setValidator(function ($email) {
                return $email;
            });
            $questions['email'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        $username = $input->getArgument('username');
        $email = $input->getArgument('email');
        $httpClient = HttpClient::create();
        $response = null;

        $response = $httpClient->request('PUT', $this->siteUrl . '/users/update/' . $id, [
            'body' => [
                'name' => $username,
                'email' => $email
            ]
        ]);

        //Check result, output success message depending on http code
        $statusCode = $response->getStatusCode();
        if ($statusCode == Response::HTTP_NOT_FOUND) {
            $output->writeln('<error>User is not found</error>');
            exit;
        }
        //return errors if we unlucky
        if ($statusCode != Response::HTTP_OK) {
            $errors = $response->getContent(false);
            $output->writeln('<error>' . $errors . '.</error>');
        } else {
            $singleUserResponse = $httpClient->request('GET', $this->siteUrl . '/users/' . $id);
            $user = json_decode($singleUserResponse->getContent());
            $table = new Table($output);
            $table->setHeaders(['id', 'name', 'email']);
            $table->setRows([[$user->id, $user->name, $user->email]]);
            $output->writeln(sprintf('<info>User has been updated:</info>'));
            $table->render();
        }
    }
}