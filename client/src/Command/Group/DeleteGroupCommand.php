<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 28.07.20
 * Time: 20:34
 */

namespace App\Command\Group;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;

class DeleteGroupCommand extends Command
{
    /**
     * @var string
     */
    private $siteUrl;

    public function __construct(string $siteUrl, $name = null)
    {
        parent::__construct($name);
        $this->siteUrl = $siteUrl;
    }

    protected static $defaultName = 'group:delete';

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Command for deleting existing group')
            ->setDefinition([
                new InputArgument('id', InputArgument::REQUIRED, 'The group id')
            ])
            ->setHelp(<<<'EOT'
                The <info>fos:user:update</info> command is deleting a group and all users that was in it
EOT
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = [];

        if (!$input->getArgument('id')) {
            $question = new Question('Enter the group id that you want to delete:');
            $question->setValidator(function ($id) {
                if (empty($id)) {
                    throw new \Exception('Group id can not be empty');
                }

                return $id;
            });
            $questions['id'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        $httpClient = HttpClient::create();
        $response = null;

        //Are you sure? There is no way to undo this action
        $helper = $this->getHelper('question');
        $question = new ConfirmationQuestion('<bg=yellow;options=bold>Are you sure? There is no way to undo this action!(y to proceed)</>', false);

        if (!$helper->ask($input, $output, $question)) {
            return 0;
        }
        //delete group request
        $response = $httpClient->request('DELETE', $this->siteUrl . '/groups/' . $id);

        //Check result, output success message depending on http code
        $statusCode = $response->getStatusCode();
        if ($statusCode == Response::HTTP_NOT_FOUND) {
            $output->writeln('<error>Group is not found</error>');
            exit;
        }

        //return errors if we unlucky
        if ($statusCode != Response::HTTP_OK) {
            $errors = $response->getContent(false);
            $output->writeln('<error>' . $errors . '.</error>');
        } else {
            $output->writeln(sprintf('<info>Group has been deleted</info>'));
        }
    }
}