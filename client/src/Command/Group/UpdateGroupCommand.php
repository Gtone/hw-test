<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 28.07.20
 * Time: 20:34
 */

namespace App\Command\Group;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;

//change name, manage group users
//add new user, delete existing
//here is your group

class UpdateGroupCommand extends Command
{
    /**
     * @var string
     */
    private $siteUrl;

    public function __construct(string $siteUrl, $name = null)
    {
        parent::__construct($name);
        $this->siteUrl = $siteUrl;
    }

    protected static $defaultName = 'group:update';

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Command for updating existing group')
            ->setDefinition([
                new InputArgument('choice', InputArgument::REQUIRED, 'The choice'),
                new InputArgument('id', InputArgument::REQUIRED, 'The user id'),
                new InputArgument('name', InputArgument::OPTIONAL, 'The username'),
                new InputArgument('userId', InputArgument::OPTIONAL, 'The user id')
            ])
            ->setHelp(<<<'EOT'
                The <info>fos:user:update</info> command is updating a user
EOT
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = [];
        //get group id
        if (!$input->getArgument('id')) {
            $question = new Question('Enter group id you want to work with:');
            $question->setValidator(function ($id) {
                if (empty($id)) {
                    throw new \Exception('Group id can not be empty');
                }

                return $id;
            });
            $questions['id'] = $question;
        }
        //multiple selections
        if (!$input->getArgument('choice')) {
            $question = new ChoiceQuestion(
                'What you want to do with group?',
                ['change name', 'delete user from group', 'add user to a group'],
                0
            );
            $question->setErrorMessage('Wrong selection');

            $choice = $this->getHelper('question')->ask($input, $output, $question);

        }
        $input->setArgument('choice', $choice);

        if ($choice === 'change name') {
            if (!$input->getArgument('name')) {
                $question = new Question('Please enter new group name:');
                $question->setValidator(function ($name) {
                    return $name;
                });
                $questions['name'] = $question;
            }
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $id = $input->getArgument('id');
        $name = $input->getArgument('name');
        $choice = $input->getArgument('choice');
        $httpClient = HttpClient::create();
        $response = null;
        //Check group existence
        $responseGroup = $httpClient->request('GET',  $this->siteUrl . '/groups/' . $id);
        //Group manage section - delete or invite users
        switch ($choice) {
            case 'add user to a group':
                $userId = $this->manageUser($responseGroup, $output, $input);
                $response = $httpClient->request('PUT', $this->siteUrl . '/groups/' . $id . '/add/' . $userId);
                break;
            case 'delete user from group':
                $userId = $this->manageUser($responseGroup, $output, $input);
                $response = $httpClient->request('DELETE', $this->siteUrl . '/groups/' . $id . '/remove/' . $userId);
                //are you sure?
                $helper = $this->getHelper('question');
                $question = new ConfirmationQuestion('<bg=yellow;options=bold>Are you sure? There is no way to undo this action!(y to proceed)</>', false);

                if (!$helper->ask($input, $output, $question)) {
                    return 0;
                }
                break;
            default:
                //change group name
                $response = $httpClient->request('PUT', $this->siteUrl . '/groups/update/' . $id, [
                    'body' => [
                        'name' => $name
                    ]
                ]);
                //Check result, output success message depending on http code
                $statusCode = $response->getStatusCode();
                if ($statusCode == Response::HTTP_NOT_FOUND) {
                    $output->writeln('<error>Group is not found</error>');
                    exit;
                }
                break;
        }
        //return errors if we unlucky
        if ($response->getStatusCode() != Response::HTTP_OK) {
            $errors = $response->getContent(false);
            $output->writeln('<error>' . $errors . '.</error>');
        } else {
            $output->writeln(sprintf('<info>Group has been updated</info>'));
            $responseGroup = $httpClient->request('GET', $this->siteUrl . '/groups/' . $id);
            $this->makeGroupsTable($responseGroup, $output);
        }
    }

    /**
     * @param $response
     * @param OutputInterface $output
     * @param $input
     * @return mixed
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * Checks user and group existence, fetching user id selection
     */
    private function manageUser($response, OutputInterface $output, $input) {
        if ($response->getStatusCode() == Response::HTTP_NOT_FOUND) {
            $output->writeln('<error>Group is not found</error>');
            exit;
        }
        $helper = $this->getHelper('question');
        $question = new Question('Enter user id:');
        $question->setValidator(function ($userId) {
            if (empty($userId)) {
                throw new \Exception('User id can not be empty');
            }
            return $userId;
        });

        $answer = $helper->ask($input, $output, $question);
        $httpClient = HttpClient::create();

        $responseUser = $httpClient->request('GET', $this->siteUrl . '/users/' . $answer);
        //Check user existence
        if ($responseUser->getStatusCode() == Response::HTTP_NOT_FOUND) {
            $output->writeln('<error>User is not found</error>');
            exit;
        }
        return $answer;
    }

    /**
     * @param $response
     * @param OutputInterface $output
     * Render group table for updated group
     */
    private function makeGroupsTable($response, OutputInterface $output)
    {
        if (!is_null($response)) {
            $groups = json_decode($response->getContent());
            $table = new Table($output);
            $table->setHeaders(['id', 'username', 'email']);
            if (!is_array($groups)) {
                $group = json_decode($response->getContent());
                $table->setHeaderTitle(sprintf('%s', $group->name));
                $rows = [];
                foreach ($group->users as $user) {
                    $rows[] = [$user->id, $user->name, $user->email];
                }
                $table->setRows($rows);
                $table->setFooterTitle(sprintf('id:%d', $group->id));
            } else {
                $rows = [];
                foreach ($groups as $group) {
                    $rows[] = [$group->id, $group->name];
                }
                $table->setRows($rows);
            }
            $table->render();
        }
    }
}