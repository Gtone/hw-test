<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 28.07.20
 * Time: 20:26
 */

namespace App\Command\Group;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;

class CreateGroupCommand extends Command
{
    /**
     * @var string
     */
    private $siteUrl;

    public function __construct(string $siteUrl, $name = null)
    {
        parent::__construct($name);
        $this->siteUrl = $siteUrl;
    }

    protected static $defaultName = 'group:create';

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Command for creating new group')
            ->setDefinition([
                new InputArgument('name', InputArgument::REQUIRED, 'The group name')
            ])
            ->setHelp(<<<'EOT'
                The <info>fos:group:create</info> command creates a group
EOT
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $questions = [];

        if (!$input->getArgument('name')) {
            $question = new Question('Please choose name for a group:');
            $question->setValidator(function ($name) {
                if (empty($name)) {
                    throw new \Exception('Group name can not be empty');
                }
                if(!is_string($name)) {
                    throw new \Exception('Wrong name format');
                }

                return $name;
            });
            $questions['name'] = $question;
        }

        foreach ($questions as $name => $question) {
            $answer = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument($name, $answer);
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name = $input->getArgument('name');
        $httpClient = HttpClient::create();
        $response = null;
        //create group request
        $response = $httpClient->request('POST', $this->siteUrl . '/groups/create', [
            'body' => [
                'name' => $name
            ]
        ]);

        //Check result, output success message depending on http code
        $statusCode = $response->getStatusCode();
        //return errors if we unlucky
        if ($statusCode != Response::HTTP_CREATED) {
            $errors = $response->getContent(false);
            $output->writeln('<error>' . $errors . '.</error>');
        } else {
            $output->writeln(sprintf('Created group <comment>%s</comment>', $name));
        }
    }
}