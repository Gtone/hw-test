<?php
/**
 * Created by PhpStorm.
 * User: niagen
 * Date: 28.07.20
 * Time: 20:34
 */

namespace App\Command\Group;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;

class GetGroupCommand extends Command
{
    /**
     * @var string
     */
    private $siteUrl;

    public function __construct(string $siteUrl, $name = null)
    {
        parent::__construct($name);
        $this->siteUrl = $siteUrl;
    }

    protected static $defaultName = 'group:get';

    protected function configure()
    {
        $this
            ->setName(self::$defaultName)
            ->setDescription('Command for updating existing group')
            ->setDefinition([
                new InputArgument('choice', InputArgument::REQUIRED, 'choice'),
                new InputArgument('id', InputArgument::OPTIONAL, 'The user id'),
            ])
            ->setHelp(<<<'EOT'
                The <info>fos:group:get</info> command is updating a group
EOT
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getArgument('choice')) {
            $question = new ChoiceQuestion(
                'Get one group or all?',
                ['one', 'all'],
                0
            );
            $question->setErrorMessage('Wrong selection');

            $choice = $this->getHelper('question')->ask($input, $output, $question);
            $input->setArgument('choice', $choice);
            if ($choice === 'one') {
                $question = new Question('Enter group id:');
                $question->setValidator(function ($id) {
                    if (empty($id)) {
                        throw new \Exception('Group id can not be empty');
                    }

                    return $id;
                });
                $answer = $this->getHelper('question')->ask($input, $output, $question);
                $input->setArgument('id', $answer);
                $input->setArgument('choice', $answer);
            }
        }
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|void|null
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $httpClient = HttpClient::create();
        $response = null;
        //If user choose id - select single group
        if (!is_null($id = $input->getArgument('id'))) {
            $response = $httpClient->request('GET', $this->siteUrl . '/groups/' . $id);

            $statusCode = $response->getStatusCode();
            if ($statusCode == Response::HTTP_NOT_FOUND) {
                $output->writeln('<error>Group is not found</error>');
                exit;
            }
            //render group table with participants
            $this->makeGroupsTable($response, $output, $id);
            exit;
        }
        $response = $httpClient->request('GET', $this->siteUrl . '/groups');
        //Check result, output success message depending on http code
        $statusCode = $response->getStatusCode();
        //return errors if we unlucky
        if ($statusCode != Response::HTTP_OK) {
            $errors = $response->getContent(false);
            $output->writeln('<error>' . $errors . '.</error>');
        }
        $this->makeGroupsTable($response, $output, $id);
    }

    /**
     * @param $response
     * @param OutputInterface $output
     * @param $id
     * Checks multiple or single group choice, render table of groups
     * or users in single group afterwards
     */
    private function makeGroupsTable($response, OutputInterface $output, $id)
    {
        if (!is_null($response)) {
            $groups = json_decode($response->getContent());
            $table = new Table($output);
            if ($id) {
                $table->setHeaders(['id', 'username', 'email']);
                $group = json_decode($response->getContent());
                $table->setHeaderTitle(sprintf('%s', $group->name));
                $rows = [];
                foreach ($group->users as $user) {
                    $rows[] = [$user->id, $user->name, $user->email];
                }
                $table->setRows($rows);
                $table->setFooterTitle(sprintf('id:%d', $group->id));
            } else {
                $table->setHeaders(['id', 'name']);
                $rows = [];
                foreach ($groups as $group) {
                    $rows[] = [$group->id, $group->name];
                }
                $table->setRows($rows);
            }
            $table->render();
        }
    }
}